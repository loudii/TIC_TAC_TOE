var tab = [
    [0,0,0],
    [0,0,0],
    [0,0,0]
];

var name_p1 = null;
var name_p2 = null;
var name1 = "";
var name2 = "";
var round = 0;
console.log(tab);
var column = tab[""];

// ************récupère les noms de l'index pour les ajouter dans l'index2**************
if (localStorage.getItem("name_p1", "name_p2") == null) {
    $('.lien_play').click(function () {
        name1 = document.getElementById("player1name").value;
        name2 = document.getElementById("player2name").value;
        localStorage.setItem("name_p1", name1);
        localStorage.setItem("name_p2", name2);
    });
}
else {
    name_p1 = localStorage.getItem("name_p1");
    name_p2 = localStorage.getItem("name_p2");

    $('#name1').html(name_p1);
    $('#name2').html(name_p2);
    localStorage.clear();
}

//**** récupère la position de la case, changer le 0 en 1 ou 2 puis y ajouter le smiley du joueur qui correspond
function getIcons(row, column){
    var boxHTML = document.querySelector(".box-" + row + "-" + column);
    var empty = boxHTML.querySelector('div');

    console.log(boxHTML);

    var box = tab[column];
    var changeNbr = box.indexOf(0);
    var player = (round % 2 == 0);
    box[changeNbr] = player + 1;

    console.log(box);

        if(empty){
            return;
        }
        if(player){
            boxHTML.innerHTML = '<div class="smiley1"></div>';
            // $('td').css({
            //     "background-color":"rgba(132, 218, 233, 0.466)"
            // })
        }
        else if(player+1){
            boxHTML.innerHTML = '<div class="smiley2"></div>';
        }
    round++
    WinCol();
    WinLine();
    WinDiago();
}
// ***********************check des gagnants en colonne******************************
function WinCol(){
    for (var i = 0; i < tab.length; i++){
        for (var j = 0; j < tab.length; j++){
            var emoji1 = tab[i][j];
            var emoji2 = tab[i][j+1];
            var emoji3 = tab[i][j+2];

            var p1Win = (
                emoji1 === 2
                && emoji2 === 2
                && emoji3 === 2
            )
            var p2Win = (
                emoji1 === 1
                && emoji2 === 1
                && emoji3 === 1
            )
            if (p1Win){
                alert('Player 1 WiiiiiiN');
                // document.location.reload(true);
            }
            if (p2Win){
                alert('Player 2 WiiiiiiN');
                // document.location.reload(true);
            }
        }
    }
}
// *****************************check des gagnants en lignes***********************
function WinLine(){
    for (var i = 0; i < tab.length; i++){
        for (var j = 0; j < tab.length; j++){
            var emoji1 = tab[i][j];
            var emoji2 = tab[i+1][j];
            var emoji3 = tab[i+2][j];

            var p1Win = (
                emoji1 === 2
                && emoji2 === 2
                && emoji3 === 2
            )
            var p2Win = (
                emoji1 === 1
                && emoji2 === 1
                && emoji3 === 1
            )
            if (p1Win){
                alert('Player 1 WiiiiiiN');
                document.location.reload(true);
            }
            if (p2Win){
                alert('Player 2 WiiiiiiN');
                document.location.reload(true);
            }
        }
    }
}
// ************************************check des gagnants en diagonale****************
function WinDiago(){
    for (var i = 0; i < tab.length; i++){
        for (var j = 0; j < tab.length; j++){
            var emoji1 = tab[i][j];
            var emoji2 = tab[i+1][j+1];
            var emoji3 = tab[i+2][j+2];

            var p1Win = (
                emoji1 === 2
                && emoji2 === 2
                && emoji3 === 2
            )
            var p2Win = (
                emoji1 === 1
                && emoji2 === 1
                && emoji3 === 1
            )
            if (p1Win){
                alert('Player 1 WiiiiiiN');
                document.location.reload(true);
            }
            if (p2Win){
                alert('Player 2 WiiiiiiN');
                document.location.reload(true);
            }
        }
    }
}